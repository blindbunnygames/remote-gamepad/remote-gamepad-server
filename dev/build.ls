import
    \livescript-system : ls
    \../.compiler.config : config

ls
    ..watch = false
    ..clean = true
    ..config = config
    ..build!
