![logo](https://gitlab.com/blindbunnygames/remote-gamepad/remote-gamepad-server/raw/master/docs/images/logo.svg.png)

Server component of RemoteGamepad that allows sharing of gaming controllers on the network. 

This is still early work in progress not production ready.

# Setup

Installation 
```
npm i remote-gamepad-server
```

Run
```
node node_modules/remote-gamepad-server
```

# Original use-case
Use any PC-compatible game controller in VR game running on mobile device.
 
![use-case](https://gitlab.com/blindbunnygames/remote-gamepad/remote-gamepad-server/raw/master/docs/images/use-case.svg.png)

# License
Source code is licensed under [BSD-3-Clause](License.md)

Other assets e.g. images are licensed under <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

