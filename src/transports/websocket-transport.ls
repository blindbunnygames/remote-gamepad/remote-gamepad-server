import
    \http
    \url
    \ws : WebSocket
    

export default wstransport = 
    max-client-inactivity-period: 10000
    init: !->
        @sockets = []
        http-server = @http-server = http.create-server!
        @ws-server = new WebSocket.Server { server: @http-server }
        _self = @
        @ws-server.on \connection (socket) !~>
            const location = url.parse socket.upgrade-req.url, true

            socket.heartbeat = Date.now!
            @sockets.push socket
            
            socket.on \message (message) !->
                socket.heartbeat = Date.now!
                console.log 'received: %s', message
            socket.on \close !->
                idx = _self.sockets.index-of @
                _self.sockets.splice idx, 1

            socket.send JSON.stringify type: 'heartbeat'
        @http-server.listen 8010, !->
            console.log 'WebSocket Server listening on %d', http-server.address!port
        set-interval @~clear-inactive-clients, 3000
      
    create: (arg) ->
        ^^@
            ..init arg
            
    broadcast: (message) !->
        for socket in @sockets
            socket.send message
    
    clear-inactive-clients: !->
        t = Date.now!
        lifespan = @max-client-inactivity-period
        @sockets := @sockets.filter -> t - it.heartbeat  < lifespan
        
        