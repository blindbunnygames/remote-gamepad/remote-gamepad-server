import
    \os
    \dgram
    \bonjour : Bonjour
    

object-filter = (object, filter-fn) ->
    result = {}
    for k,v of object when filter-fn v, k, object
        result[k] = v
    result
    
export default udp-transport = 
    max-client-inactivity-period: 10000
    init: !->
        @clients = {}
        server = @server = dgram.create-socket \udp4
        @server.on \listening !->
            address = server.address!
            try
                bonjour = Bonjour!
                process.on \exit !->
                    bonjour.unpublish-all!
                    bonjour.destroy!
                bonjour.publish name: "Remote Gamepad at #{os.hostname!}", type: 'remote-input', port: address.port
            catch
                console.log "Couldn't register service"
                console.log e

            console.log "UDP Server listening on #{address.address}:#{address.port}"
            
        client-send = ->
            server.send do
                it
                @ip-port
                @ip-address
        
        @server.on \message (raw-message, remote) !~>
            console.log "#{remote.address}:#{remote.port} - '#{raw-message}'"
            try
                message = JSON.parse raw-message
                if message?type == \discovery
                    @on-discovery message, remote
                @clients{}[remote.address]
                    ..heartbeat = Date.now!
                    ..ip-address = remote.address
                    ..ip-port = remote.port
                    ..send = client-send
        @server.bind 33333
        set-interval @~clear-inactive-clients, 3000

    clear-inactive-clients: !->
        t = Date.now!
        lifespan = @max-client-inactivity-period
        @clients := object-filter @clients, (client) -> t - client.heartbeat  < lifespan
    
    on-discovery: (message, remote) !->
        @server.send do
            JSON.stringify type: \discovery.response
            remote.port
            remote.address
        
    broadcast: (message) !->
        for ,client of @clients
            client.send message

    create: (arg) ->
      ^^@
          ..init arg
    