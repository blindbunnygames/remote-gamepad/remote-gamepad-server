export default mapping = 
    name: 'Microsoft X-Box One S pad'
    axis:
        0:
            id: 0
            direction: 1
        1:
            id: 1
            direction: -1
        2:
            id: 4
            direction: 1
        3:
            id: 2
            direction: 1
        4:
            id: 3
            direction: -1