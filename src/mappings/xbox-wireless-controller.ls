export default mapping =
    name: 'Xbox Wireless Controller'
    axis:
        0:
            id: 0
            direction: 1
        1:
            id: 1
            direction: -1
        3:
            id: 3
            direction: -1