require! {
    'dgram'
    'bonjour': Bonjour
}

bonjour = Bonjour!

bonjour.find {type: 'remote-input'} (service) !->
    console.log 'Input Server at:', service
    server-id =
        address: service.addresses.0
        port: service.port

    console.log server-id
    client = dgram.create-socket \udp4

    const heartbeat-message = JSON.stringify type: \heartbeat client: \nodes.js

    heartbeat = !->
        client.send heartbeat-message, server-id.port, server-id.address

    client.on \listening !->
        address = client.address!
        console.log 'UDP Client listening on ' + address.address + ":" + address.port
        client.send "hello", server-id.port, server-id.address
        set-interval heartbeat, 2000

    client.on \message (message, remote) !->
        console.log "#{remote.address}:#{remote.port} - '#{message}'"

    client.bind!
