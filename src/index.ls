import
    'dgram'
    'bonjour': Bonjour
    'gamepad'
    'http'
    'ws': WebSocket
    'url'
    \./transports/udp-transport
    \./transports/websocket-transport

import all
    \./mappings


# Initialize the library
gamepad.init!
transports = []
    ..push udp-transport.create!
    ..push websocket-transport.create!

# List the state of all currently attached devices
for i from 0 til gamepad.num-devices!
    console.log i, gamepad.device-at-index!


# Create a game loop and poll for events
setInterval gamepad.process-events, 16
# Scan for new gamepads as a slower rate
setInterval gamepad.detect-devices, 500

input-map = {}
for k,v of mappings
    input-map[v.name] = v
  
const PORT = 33333
const HOST = null#'127.0.0.1'

websocket-clients = []

broadcast = (message) !->
    for t in transports
        t.broadcast message

const heartbeat-message = JSON.stringify type: \heartbeat

heartbeat = !-> broadcast heartbeat-message

# Listen for move events on all gamepads
gamepad.on \move (id, axis, value) !->
    if (device-map = input-map[ gamepad.deviceAtIndex!description])
    and mapped-axis = device-map.axis[axis]
        broadcast JSON.stringify type: \move, data: { id, axis:mapped-axis.id, value: value*mapped-axis.direction }
    else
        broadcast JSON.stringify type: \move, data: { id, axis, value }

# Listen for button up events on all gamepads
gamepad.on \up (id, num) !->
    broadcast JSON.stringify type: \up, data: { id, num }

# Listen for button down events on all gamepads
gamepad.on \down (id, num) !->
    broadcast JSON.stringify type: \down, data: { id, num }

